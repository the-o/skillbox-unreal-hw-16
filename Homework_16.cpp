﻿#include <iostream>
#include <time.h>

const int N = 25;

int main()
{
    int arr[N][N];

    for (int i = 0; i < N; ++i) 
    {
        for (int j = 0; j < N; ++j) 
        {
            arr[i][j] = i + j;
        }
    }

    for (int i = 0; i < N; ++i)
    {
        for (int j = 0; j < N; ++j)
        {
            std::cout << arr[i][j] << " ";
        }
        std::cout << std::endl;
    }

    time_t t = time(NULL);
    struct tm tm;
    localtime_s(&tm, &t);
    int day = tm.tm_mday;
    
    int line_idx = day % N;
    int sum = 0;

    for (int j = 0; j < N; ++j) 
    {
        sum += arr[line_idx][j];
    }

    std::cout << sum << std::endl;
}

